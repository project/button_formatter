<?php

namespace Drupal\button_formatter\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ButtonFormatterSettings.
 */
class ButtonFormatterSettings extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ButtonFormatterSettings object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config,
    EntityTypeManager $entity_type_manager,
  ) {
    parent::__construct($config_factory , $typed_config);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory'),
          $container->get('config.typed'),
          $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'button_formatter.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'button_formatter_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('button_formatter.settings');

    $form['global_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Global Class"),
      '#default_value' => $config->get('global_class'),
    ];

    $form['styles'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Styles for Button Formatter"),
      '#default_value' => implode(PHP_EOL, $config->get('styles')),
    ];

    $form['sizes'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Sizes for Button Formatter"),
      '#default_value' => implode(PHP_EOL, $config->get('sizes') ?? []),
    ];

    $form['radius'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Radius for Button Formatter"),
      '#default_value' => implode(PHP_EOL, $config->get('radius') ?? []),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $styles = $form_state->getValue('styles');
    $styles = explode(PHP_EOL, $styles);

    foreach ($styles as $style) {
      $class_label = explode('|', $style);
      if (count($class_label) != 2) {
        $form_state->setErrorByName('styles', $this->t("Invalid format for button styles"));
        return;
      }
    }

    $sizes = $form_state->getValue('sizes');
    $sizes = explode(PHP_EOL, $sizes);

    foreach ($sizes as $size) {
      $class_label = explode('|', $size);
      if (count($class_label) != 2) {
        $form_state->setErrorByName('sizes', $this->t("Invalid format for button sizes"));
        return;
      }
    }

    $radius = $form_state->getValue('radius');
    $radius = explode(PHP_EOL, $radius);

    foreach ($radius as $item) {
      $class_label = explode('|', $item);
      if (count($class_label) != 2) {
        $form_state->setErrorByName('radius', $this->t("Invalid format for button radius"));
        return;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('button_formatter.settings');
    $values = $form_state->getValues();

    // Global Class.
    $config->set('global_class', $values['global_class']);

    // Button Styles.
    $config->set('styles', explode(PHP_EOL, str_replace("\r", "", $values['styles'])));

    // Button Sizes.
    $config->set('sizes', explode(PHP_EOL, str_replace("\r", "", $values['sizes'])));

    // Button Sizes.
    $config->set('radius', explode(PHP_EOL, str_replace("\r", "", $values['radius'])));

    // Save the config.
    $config->save();
  }

}
