<?php

namespace Drupal\button_formatter\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'button_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "button_formatter",
 *   label = @Translation("Button"),
 *   field_types = {
 *     "file",
 *     "link"
 *   }
 * )
 */
class ButtonFormatter extends FormatterBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The button formatter settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Construct a button_formatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entityManager
   *   Entity manager service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('button_formatter.settings');
  }

  /**
   * {@inheritDoc}.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'style' => NULL,
      'show_icon' => TRUE,
      'icon' => '<i class="fa fa-file" aria-hidden="true"></i>',
      'download' => FALSE,
      'new_tab' => FALSE,
      'show_custom_label' => FALSE,
      'custom_label' => '',
      'show_description' => FALSE,
      'is_external' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();

    $form['style'] = [
      '#type' => 'select',
      '#title' => $this->t("Style"),
      '#options' => $this->getStyleOptions(),
      '#default_value' => $settings['style'],
      '#required' => TRUE,
    ];

    $form['is_external'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Check if url is external"),
      '#attributes' => [
        'id' => 'is_external',
      ],
      '#default_value' => $settings['is_external'],
    ];

    $form['show_icon'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Show icon"),
      '#default_value' => $settings['show_icon'],
      '#attributes' => [
        'id' => 'show-icon',
      ],
    ];

    $form['icon'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Icon markup"),
      '#default_value' => $settings['icon'],
      '#states' => [
        'visible' => [
          '#show-icon' => ['checked' => TRUE],
        ],
      ],
    ];

    if ($this->fieldDefinition->getType() == "file") {
      $form['download'] = [
        '#type' => 'checkbox',
        '#title' => $this->t("Download"),
        '#attributes' => [
          'id' => 'download',
        ],
        '#states' => [
          'visible' => [
            '#new_tab' => ['checked' => FALSE],
          ],
        ],
        '#default_value' => $settings['download'],
      ];
    }

    $form['new_tab'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Open in new tab"),
      '#attributes' => [
        'id' => 'new_tab',
      ],
      '#states' => [
        'visible' => [
          '#download' => ['checked' => FALSE],
        ],
      ],
      '#default_value' => $settings['new_tab'],
    ];

    $form['show_custom_label'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Show Custom Label?"),
      '#attributes' => [
        'id' => 'show-custom-label',
      ],
      '#default_value' => $settings['show_custom_label'],
      '#states' => [
        'visible' => [
          '#show-description' => ['checked' => FALSE],
        ],

      ],
    ];

    $form['custom_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Custom Label"),
      '#states' => [
        'visible' => [
          '#show-custom-label' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $settings['custom_label'],
    ];

    if ($this->fieldDefinition->getType() == "file") {
      $form['custom_label']['#states']['visible']['#show-description'][] = "or";
      $form['custom_label']['#states']['visible']['#show-description'] = ['checked' => FALSE];
      $form['show_description'] = [
        '#type' => 'checkbox',
        '#title' => $this->t("Show Description field as a label?"),
        '#attributes' => [
          'id' => 'show-description',
        ],
        '#default_value' => $settings['show_description'],
        '#states' => [
          'visible' => [
            '#show-custom-label' => ['checked' => FALSE],
          ],
        ],
      ];
    }

    $form['no_custom_label'] = [
      '#type' => 'html_tag',
      '#tag' => 'small',
      '#value' => $this->t(
        '
        If "Show Custom Label?" is not checked, the formatter will determine the
        label of the link by itself, usually by getting the current entity label or the link title. Compatible with media entities.
      '
      ),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    $summary['style'] = $this->t("<strong>Style</strong>: @style", ['@style' => $settings['style']]);
    $summary['show_icon'] = $this->t(
      "<strong>Show Icon?</strong>: @result",
      ['@result' => (bool) $settings['show_icon'] ? $this->t("Yes") : $this->t("No")]
    );
    $summary['download'] = $this->t(
      "<strong>Download</strong>: @result",
      ['@result' => (bool) $settings['download'] ? $this->t("Yes") : $this->t("No")]
    );
    $summary['new_tab'] = $this->t(
      "<strong>Open in New Tab</strong>: @result",
      ['@result' => (bool) $settings['new_tab'] ? $this->t("Yes") : $this->t("No")]
    );
    $summary['show_description'] = $this->t(
      "<strong>Show Description?</strong>: @result",
      ['@result' => (bool) $settings['show_description'] ? $this->t("Yes") : $this->t("No")]
    );
    $summary['is_external'] = $this->t(
      "<strong>Is external</strong>: @result",
      ['@result' => (bool) $settings['is_external'] ? $this->t("Yes") : $this->t("No")]
    );

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'button_link',
        '#button' => Link::fromTextAndUrl(
          $this->getLabel($item),
          $this->getUrl($item)
        ),
        '#entity' => $items->getEntity(),
        '#field_name' => $items->getFieldDefinition()->getName(),
      ];
    }

    return $elements;
  }

  /**
   * Gets the url.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *
   * @return \Drupal\Core\Url
   */
  protected function getUrl(FieldItemInterface $item) {
    $url = NULL;
    $settings = $this->getSettings();

    if ($this->fieldDefinition->getType() == "file") {
      $file_url = $this->getFile($item->getValue()['target_id'])->createFileUrl(FALSE);
      $url = Url::fromUri($file_url);
    }
    else {
      $url = Url::fromUri($item->getValue()['uri']);
    }

    // Set the attributes of the url.
    $url->setOption(
      'attributes', [
        'class' => $this->getClass($url->isExternal()),
        'download' => (bool) $settings['download'],
        'target' => (bool) $settings['new_tab'] ? "_blank" : NULL,
      ]
    );

    return $url;
  }

  /**
   * Gets the icon of the button.
   *
   * @return array
   */
  protected function getIcon() {
    $settings = $this->getSettings();

    if ((bool) $settings['show_icon']) {
      return $settings['icon'] . Html::decodeEntities("&nbsp;");
    }

    return NULL;
  }

  /**
   * Gets a file entity.
   *
   * @param int $file_id
   *   The file id.
   *
   * @return \Drupal\file\FileInterface
   */
  protected function getFile($file_id) {
    return $this->entityTypeManager->getStorage('file')->load($file_id);
  }

  /**
   * Gets the label of the button.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *
   * @return string
   */
  protected function getLabel(FieldItemInterface $item) {
    $settings = $this->getSettings();
    $label = "";

    if ((bool) $settings['show_custom_label']) {
      $label = $this->t($settings['custom_label']);
    }
    else {
      if ($this->fieldDefinition->getType() == "file") {
        $entity = $item->getEntity();

        if ((bool) $settings['show_description']) {
          // Return the description.
          $label = $item->getValue()['description'];
        }
        else {
          // If the current entity is media, we get its name.
          if ($entity->getEntityTypeId() == "media") {
            $label = $entity->label();
          }
          else {
            // Else, we get the file name.
            $file = $this->getFile($item->getValue()['target_id']);
            $label = $file->label();
          }
        }
      }
      else {
        // If it is a link field, we get the text.
        $label = $item->getValue()['title'];
      }
    }

    if ((bool) $settings['show_icon']) {
      $label = Markup::create($this->getIcon() . Html::escape($label));
    }

    return $label;
  }

  /**
   * Gets the style options from the button formatter settings form.
   *
   * @return array
   */
  protected function getStyleOptions() {
    $style_options = [];
    $styles = $this->config->get('styles');
    foreach ($styles as $style) {
      $fragments = explode('|', $style);
      $style_options[$fragments[0]] = $fragments[1];
    }

    return $style_options;
  }

  /**
   * Gets the class of the formatter.
   *
   * @return string
   */
  protected function getClass($isExternal = FALSE) {
    $settings = $this->getSettings();
    $external_link_class = '';
    // Add the 'external-link' class if the URL is external and the 'is_external' setting is enabled.
    if ($isExternal && $settings['is_external']) {
      $external_link_class = empty($this->config->get('external_link')) ? 'external-link' : $this->config->get('external_link');
    }
    return [
      $this->config->get('global_class'),
      $this->getSetting('style'),
      $external_link_class,
    ];
  }

}
